# Jeu de Morpion!

## Régle du jeu :

Le Morpion, également appelé Tic-Tac-Toe se joue sur une grille carrée de **3x3** cases. Deux joueurs, vous et l'ordinateur s'affrontent. Ils doivent remplir chacun à leur tour une case de la grille avec le symbole qui leur est attribué : **O** ou **X**.  
  
Le gagnant est celui qui arrive à aligner trois symboles identiques, horizontalement, verticalement ou en diagonale.